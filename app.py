from flask import Flask

import config

app = Flask(__name__)

@app.route("/")
def hello():
    return "V4.0 Flask App, Cloud Build, GCR, GKE V.4.!"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=config.PORT, debug=config.DEBUG_MODE)
